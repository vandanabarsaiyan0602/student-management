public class StudentManagement {
    public static void main(String[] args) {
        Student s1 = new Student(1, "X", "London");
        Student s2 = new Student(2, "Y", "London");
        Student s3 = new Student(3, "Z", "Brazil");
        Admin admin = new Admin();
        admin.addStudent(s1);
        admin.addStudent(s2);
        admin.addStudent(s3);
        admin.deleteStudent(s1);
        admin.updateStudent(2,new Student(4,"Anjali","India"));
        admin.display();
        Staff staff=new Staff();
        staff.display();
    }
}
