public class Student {
    private int rollNumber;
    private String name;
    private String address;
    public Student(int rollNumber,String name,String address){
        this.rollNumber=rollNumber;
        this.name=name;
        this.address=address;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }
}
