import java.lang.reflect.Array;
import java.util.ArrayList;

public class Admin{

    private static ArrayList<Student> studentList;
    public Admin(){
        this.studentList=new ArrayList<>();
    }
    public void addStudent(Student s){
        studentList.add(s);
        System.out.println("Student "+s.getName()+" added in records");
    }
    public void deleteStudent(Student s){
        studentList.remove(s);
        System.out.println("Student "+s.getName()+" deleted from records");
    }
    public void updateStudent(int studentNumber,Student newStudent){
        int index=studentNumber-1;
        studentList.set(index,newStudent);
        System.out.println("Student "+newStudent.getName()+" updated from records");
    }
    protected void display(){
        for(Student s:studentList)
            System.out.println(s);
    }


}
